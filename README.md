# Generic API DAST E2E Test project

Use to include new E2E tests in API Fuzzing project.

1. Configure DAST E2E Test project
   1. If needed add resources into directory `assets/<your-project>`
1. Add a new job in the API Fuzzing project 

<!-- 
e2e_s_dast_har_cwe_heartbleed
e2e_s_dast_graphql_har
 -->